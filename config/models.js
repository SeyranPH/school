module.exports.models = {
  datastore: 'mongodb',
  attributes: {
    createdAt: { type: 'number', autoCreatedAt: true, },
    updatedAt: { type: 'number', autoUpdatedAt: true, },
    id: { type: 'string', columnName: '_id' },
  },
  dataEncryptionKeys: {
    default: '9jy1xz9/rGea/8lUZQlXQUNMAFpFiSv5DMPrrOqfZws='
  },
  cascadeOnDestroy: true
};
