module.exports.routes = {

  'post /signup': 'AuthController.signUp',
  'post /login': 'AuthController.login',

};
