module.exports.datastores = {
    default: {
        adapter: 'sails-mongo',
        url: 'mongodb://127.0.0.1:27017/school'
    }
};