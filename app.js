const sails = require('sails');
const app = new sails.constructor();
app.lift({  port: 3000  }, function(err) {
  if (err) {
    console.error('Failed to lift app.  Details: ' + err);
    return;
  }
});