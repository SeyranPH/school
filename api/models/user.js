module.exports = {
    attributes: {
        name: "STRING",
        age: "INTEGER",
        email: "STRING",

        username: "STRING",
        password: "STRING",
        isTeacher: "BOOLEAN"
    }
}
