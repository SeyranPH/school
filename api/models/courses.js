module.exports = {
    attribute: {
        name: "STRING",
        teacher: "STRING",
        students: {
            studentName: "STRING",
            grade: "NUMBER"
        }
    }
}