const authenticate = function(req, res){
    if (!req.session.authenticated) {
		return res.send('Permission denied',403);
	}
};

module.exports = authenticate;